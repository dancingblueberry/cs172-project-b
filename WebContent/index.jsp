<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.io.*,java.util.*, java.util.regex.*, edu.ucr.*" %>
<%@page session="true"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<link rel="stylesheet" href="${pageContext.request.contextPath}/custom.css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> CS172 Project Demo </title>

</head>
<body topmargin="0" leftmargin="10" rightmargin="10" bottommargin="0">
<% 
	State state = State.NONE; 
	String queryString = "", reIndex = "";
	Integer answerSize = 10;
	if (request.getParameter("search_submit") != null) {
		queryString = request.getParameter("queryString"); 
		state = State.SEARCH; 
	} else if (request.getParameter("re_index") != null) {
		state = State.INDEX; 
	}
%>
<div id="page">
	<div id="masthead">
		<font size="16" color="#ffffff">CS172 Project Demo</font>
	</div>
	<div id="container">
		<div id="form">
			<form action="index.jsp" method="POST">
				<br />
				<input type="text" name="queryString" value="<% out.println(queryString); %>" size="40" /> 
				<input type="submit" value="Search" name="search_submit" class="button" />
				<input type="submit" value="Index" name="re_index" class="button secondary" />
			</form>
		</div>
		<div id="contents">		
			<div id="results_area">
					<%
						switch(state)
						{
							case SEARCH: 
								SearchEngine searchEngine = new SearchEngine();
								ArrayList<WebDocument> topDocs = searchEngine.search(queryString, 10);
								
								if (topDocs != null)
								{
									/* if (answerSize > 1) {
										out.print("Results for: " + queryString); 
										 
										out.print("<hr />");
									} */
									for(WebDocument webDoc : topDocs) {	
										out.print("<div class=\"results\">");
											out.print("<div class=\"title\"><a href=\"" + webDoc.url + "\" target=\"_blank\">" + webDoc.title +"</a></div>");
											out.print("<div class=\"score\">Score: " + webDoc.score + "</div>"); 
											out.print("<div class=\"snippet\">" + webDoc.body + "...</div>");
										out.print("</div><br />"); 	
									}
								}
								break; 
							case INDEX: 
								Indexer indexer = new Indexer(); 
								indexer.indexDir(); 
								out.print("<div>Finished re-indexing</div>");
								break; 
							default: 
								break; 
						}
					%>
			</div>
		</div>
	</div>
</div>
</body>
</html>
