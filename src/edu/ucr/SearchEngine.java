package edu.ucr;

import java.io.*;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.HashSet;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;


public class SearchEngine
{
  public static final String INDEX_DIR = "testIndex";

  public ArrayList<WebDocument> search (String queryString, int topk) throws CorruptIndexException, IOException 
  {
    IndexReader indexReader = IndexReader.open(FSDirectory.open(new File(INDEX_DIR)), true);
    IndexSearcher indexSearcher = new IndexSearcher(indexReader);
    QueryParser queryparser = new QueryParser(Version.LUCENE_34, "text", new StandardAnalyzer(Version.LUCENE_34));
    ArrayList<WebDocument> webDocuments = new ArrayList<WebDocument>();
    HashSet<String> duplicates = new HashSet();
    
    try 
    {
      StringTokenizer strtok = new StringTokenizer(queryString, " ~`!@#$%^&*()_-+={[}]|:;'<>,./?\"\'\\/\n\t\b\f\r");
      String querytoparse = "";
      while(strtok.hasMoreElements()) 
      {
        String token = strtok.nextToken();
        querytoparse += "text:" + token + "^1" + "title:" + token + "^1.5";
      } 
      Query query = queryparser.parse(querytoparse);
      TopDocs results = indexSearcher.search(query, topk);
      int maxSnippet = 500; 
      
      for (ScoreDoc scoreDoc : results.scoreDocs )
      {
    	Document doc = indexSearcher.doc(scoreDoc.doc);
    	String url = doc.getFieldable("url").stringValue();
    	if(!duplicates.contains(url))
    	{
    		duplicates.add(url);
	    	String body = doc.getFieldable("text").stringValue();
	    	if (body.length() > maxSnippet)
	    		body = body.substring(0, maxSnippet);
	    	String title = doc.getFieldable("title").stringValue();
	    	float score = scoreDoc.score; 
	    	WebDocument webDocument = new WebDocument(url, title, body, score);
	    	webDocuments.add(webDocument);
    	}
      }

      return webDocuments; 
    } 
    catch (Exception e) 
    {
      e.printStackTrace();
    } 
    finally 
    {
      indexSearcher.close();
    }
    return null;
  }
}

