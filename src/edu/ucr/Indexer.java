package edu.ucr; 

import java.io.*;
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

public class Indexer {
	public static final String INDEX_DIR = "testIndex";
	public static final String FILE_DIR = "output";
	
	public static void indexDir() 
	  {
	    File index = null; 
	    IndexWriterConfig indexConfig = null; 
	    IndexWriter writer = null; 

	    try
	    {
	      delete(new File(INDEX_DIR));
	      index = new File(INDEX_DIR);
	      indexConfig = new IndexWriterConfig(Version.LUCENE_34, new StandardAnalyzer(Version.LUCENE_35));
	      writer = new IndexWriter(FSDirectory.open(index), indexConfig);
	    }
	    catch (Exception e)
	    {
	      System.out.println(e.getMessage()); 
	      return; 
	    }

	    File[] files = new File(FILE_DIR).listFiles();
	    for (File file : files) 
	    {
	      // System.out.println("File: " + file.getName()); 
	      try
	      {
	        BufferedReader br = new BufferedReader(new FileReader(file)); 
	        while (br.ready())
	        {
	          String url = br.readLine(); 
	          String title = br.readLine(); 
	          String body = br.readLine(); 

	          System.out.println("Indexing: " + file.getName()); 
	          // System.out.println("Url: " + url); 
	          // System.out.println("Title: " + title); 
	          // System.out.println("Body: " + body); 
	          // System.out.println();
	          WebDocument page = new WebDocument(url, title, body); 
	          indexPage(page, writer, index); 
	        }
	        br.close();
	      }
	      catch (Exception e)
	      {
	        //System.out.println(e.getMessage()); 
	      }
	    }

	    try 
	    {
	      writer.close();
	    } 
	    catch (CorruptIndexException e) 
	    {
	      //e.printStackTrace();
	    } 
	    catch (IOException e) 
	    {
	      //e.printStackTrace();
	    }
	  }
	  
	  public static void indexPage (WebDocument page, IndexWriter writer, File index) 
	  {
	    try 
	    { 
	      //System.out.println("Indexing to directory '" + index + "'..."); 
	      Document luceneDoc = new Document();  
	      luceneDoc.add(new Field("text", page.body, Field.Store.YES, Field.Index.ANALYZED));
	      luceneDoc.add(new Field("url", page.url, Field.Store.YES, Field.Index.NO));
	      luceneDoc.add(new Field("title", page.title, Field.Store.YES, Field.Index.ANALYZED));
	      writer.addDocument(luceneDoc);      
	    } 
	    catch (Exception ex) 
	    {
	      //ex.printStackTrace();
	    }
	  }
	  
	  private static void delete(File f) throws IOException
	  {
		  if (f.isDirectory())
		  {
			  for(File c : f.listFiles())
				  delete(c); 
		  }
		  if (!f.delete())
		  {
			  throw new FileNotFoundException("Failed to delete file: " + f); 
		  }
	  }
}
