package edu.ucr;

public class WebDocument 
{
  public String url;  
  public String title;
  public String body;
  public float score; 

  public WebDocument(String u, String t, String b) 
  {
    this.url = u;
    this.title = t;
    this.body = b;
    this.score = -1; 
  }
  
  public WebDocument(String u, String t, String b, float score) 
  {
    this.url = u;
    this.title = t;
    this.body = b;
    this.score = score; 
  }
}
